package interpreter;

public class Soma implements Expressao {

    private final Expressao esquerda;
    private final Expressao direita;

    public Soma(Expressao esquerda, Expressao direita) {
        this.esquerda = esquerda;
        this.direita = direita;
    }

    @Override
    public int avalia() {
        int esquerdaResultado = esquerda.avalia();
        int direitaResultado = direita.avalia();
        return esquerdaResultado + direitaResultado;
    }

}
