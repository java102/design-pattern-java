package interpreter;

public class Divisao implements Expressao {

    private final Expressao esquerda;
    private final Expressao direita;

    public Divisao(Expressao esquerda, Expressao direita) {
        this.esquerda = esquerda;
        this.direita = direita;
    }

    @Override
    public int avalia() {
        int esquerdaResultado = esquerda.avalia();
        int direitaResultado = direita.avalia();
        return esquerdaResultado / direitaResultado;
    }

}
