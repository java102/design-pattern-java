package state;

public interface EstadoDeUmOrcamento {

    // aplica desconto extra conforme o estado atual
    void aplicaDescontoExtra(Orcamento orcamento);

    void aprova(Orcamento orcamento);

    void reprova(Orcamento orcamento);

    void finaliza(Orcamento orcamento);

}
