package state;

public class TesteState {

    public static void main(String[] args) {
        Orcamento reforma = new Orcamento(1500.0);

        reforma.aplicaDescontoExtra();
        System.out.println(reforma.getValor());

        reforma.aprova();
        reforma.aplicaDescontoExtra();
        System.out.println(reforma.getValor());

        reforma.finaliza();
    }

}
