package observer2;

import java.util.ArrayList;
import java.util.List;

public class DadosSubject  {

    protected List<DadosObserver> observers;
    protected Dados dados;

    public DadosSubject() {
        this.observers = new ArrayList<>();
    }

    public void setState(final Dados dados) {
        this.dados = dados;
        notifyObservers();
    }

    private void notifyObservers() {
        for (final DadosObserver observer : this.observers) {
            observer.update();
        }
    }

    public Dados getState() {
        return dados;
    }

    public void attach(final DadosObserver observer) {
        this.observers.add(observer);
    }

    public void detach(final Integer indice) {
        observers.remove(indice);
    }

    public void detach(final DadosObserver observer) {
        observers.remove(observer);
    }

}
