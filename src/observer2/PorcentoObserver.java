package observer2;

import java.text.DecimalFormat;

public class PorcentoObserver extends DadosObserver {

    public PorcentoObserver(DadosSubject dadosSubject) {
        super(dadosSubject);
    }

    @Override
    public void update() {
        int somaDosValores = this.dadosSubject.getState().getValorA() + this.dadosSubject.getState().getValorB()
                + this.dadosSubject.getState().getValorC();
        DecimalFormat formatador = new DecimalFormat("#.##");
        String porcentagemA = formatador.format((double) this.dadosSubject.getState().getValorA()
                / somaDosValores);
        String porcentagemB = formatador.format((double) this.dadosSubject.getState().getValorB()
                / somaDosValores);
        String porcentagemC = formatador.format((double) this.dadosSubject.getState().getValorC()
                / somaDosValores);
        System.out.println("Porcentagem:\nValor A: " + porcentagemA
                + "%\nValor B: " + porcentagemB + "%\nValor C: " + porcentagemC
                + "%");
    }

}
