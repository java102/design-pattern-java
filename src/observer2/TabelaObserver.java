package observer2;

public class TabelaObserver extends DadosObserver {

    public TabelaObserver(DadosSubject dadosSubject) {
        super(dadosSubject);
    }

    @Override
    public void update() {
        System.out.println("Tabela:\nValor A: " + this.dadosSubject.getState().getValorA()
                + "\nValor B: " + this.dadosSubject.getState().getValorB() + "\nValor C: "
                + this.dadosSubject.getState().getValorC());
    }

}
