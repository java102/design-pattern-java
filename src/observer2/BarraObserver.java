package observer2;

public class BarraObserver extends DadosObserver {

    public BarraObserver(DadosSubject dadosSubject) {
        super(dadosSubject);
    }

    @Override
    public void update() {
        String barraA = "", barraB = "", barraC = "";

        for (int i = 0; i < this.dadosSubject.getState().getValorA(); i++) {
            barraA += '=';
        }

        for (int i = 0; i < this.dadosSubject.getState().getValorB(); i++) {
            barraB += '=';
        }

        for (int i = 0; i < this.dadosSubject.getState().getValorC(); i++) {
            barraC += '=';
        }

        System.out.println("Barras:\nValor A: " + barraA + "\nValor B: "
                + barraB + "\nValor C: " + barraC);
    }

}
