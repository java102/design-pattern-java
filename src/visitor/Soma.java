package visitor;

public class Soma implements Expressao {

    private final Expressao esquerda;
    private final Expressao direita;

    public Soma(Expressao esquerda, Expressao direita) {
        this.esquerda = esquerda;
        this.direita = direita;
    }

    @Override
    public int avalia() {
        int esquerdaResultado = esquerda.avalia();
        int direitaResultado = direita.avalia();
        return esquerdaResultado + direitaResultado;
    }

    @Override
    public void aceita(Visitor visitor) {
        visitor.visitaSoma(this);
    }

    public Expressao getEsquerda() {
        return esquerda;
    }

    public Expressao getDireita() {
        return direita;
    }

}
