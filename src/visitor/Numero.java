package visitor;

public class Numero implements Expressao {

    private Integer numero;

    public Numero(Integer numero) {
        this.numero = numero;
    }

    @Override
    public int avalia() {
        return numero.intValue();
    }

    @Override
    public void aceita(Visitor visitor) {
        visitor.visitaNumero(this);
    }

    public Integer getNumero() {
        return numero;
    }

}
