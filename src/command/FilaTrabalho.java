package command;

import java.util.ArrayList;
import java.util.List;

public class FilaTrabalho {

    private List<Command> comandos;

    public FilaTrabalho() {
        this.comandos = new ArrayList<>();
    }

    public void adiciona(Command command) {
        this.comandos.add(command);
    }

    public void processa() {
        for (Command comando : this.comandos) {
            comando.executa();
        }
    }

}
