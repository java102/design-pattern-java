package command;

import java.util.Calendar;

public class Pedido {

    private String cliente;
    private double valor;
    private Status status;
    private Calendar dataFinalizacao;


    public Pedido(String cliente, double valor) {
        this.cliente = cliente;
        this.valor = valor;
    }

    public void paga() {
        System.out.println("pagando pedido " + cliente);
        status = Status.PAGO;
    }

    public void finaliza() {
        System.out.println("finalizando pedido " + cliente);
        dataFinalizacao = Calendar.getInstance();
        status = Status.ENTREGUE;
    }

}
