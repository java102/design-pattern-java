package template_method;

public class Teste {

    public static void main(String[] args) {
        Orcamento orcamento = new Orcamento(1500.0);
        Imposto icpp = new ICPP();

        new CalculadorDeImposto().calcula(orcamento, icpp);
    }

}
