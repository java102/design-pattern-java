package observer;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class NotaFiscalBuilder {

    private String razaoSocial;
    private String cnpj;
    private double valorBruto;
    private double impostos;
    private Calendar data;
    private String observacoes;
    private List<ItemDaNota> itens = new ArrayList<>();
    private List<AcaoAposGerarNota> acoes;

    public NotaFiscalBuilder() {
        this.data = Calendar.getInstance(); // campo obrigatório
        this.acoes = new ArrayList<>();
    }

    public NotaFiscalBuilder paraEmpresa(String razaoSocial) {
        this.razaoSocial = razaoSocial;
        return this;
    }

    public NotaFiscalBuilder comCnpj(String cnpj) {
        this.cnpj = cnpj;
        return this;
    }

    public NotaFiscalBuilder comItem(ItemDaNota item) {
        itens.add(item);
        valorBruto += item.getValor();
        impostos += item.getValor() * 0.05;
        return this;
    }

    public NotaFiscalBuilder naData(Calendar data) {
        this.data = data;
        return this;
    }

    public NotaFiscalBuilder naDataAtual() {
        this.data = Calendar.getInstance();
        return this;
    }

    public NotaFiscalBuilder comObservacoes(String observacoes) {
        this.observacoes = observacoes;
        return this;
    }

    public void adicionaAcao(AcaoAposGerarNota acao) {
        this.acoes.add(acao);
    }

    public NotaFiscal build() {
        NotaFiscal nf = new NotaFiscal(razaoSocial, cnpj, data, valorBruto, impostos, itens, observacoes);

        for (AcaoAposGerarNota acao : this.acoes) {
            acao.executa(nf);
        }

        return nf;
    }

}
