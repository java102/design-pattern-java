package observer;

public class ItemDaNota {

    private final String nome;
    private final Double valor;

    public ItemDaNota(String nome, Double valor) {
        this.nome = nome;
        this.valor = valor;
    }

    public String getNome() {
        return nome;
    }

    public Double getValor() {
        return valor;
    }

    @Override
    public String toString() {
        return "ItemDaNota{" +
                "nome='" + nome + '\'' +
                ", valor=" + valor +
                '}';
    }

}
