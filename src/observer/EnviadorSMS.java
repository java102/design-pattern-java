package observer;

public class EnviadorSMS implements AcaoAposGerarNota {

    @Override
    public void executa(NotaFiscal nf) {
        System.out.println("Enviando SMS");
    }

}
