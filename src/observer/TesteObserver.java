package observer;

public class TesteObserver {

    public static void main(String[] args) {
        NotaFiscalBuilder builder = new NotaFiscalBuilder();
        builder.adicionaAcao(new EnviadorEmail());
        builder.adicionaAcao(new NotaFiscalDAO());
        builder.adicionaAcao(new EnviadorSMS());
        builder.adicionaAcao(new Impressora());

        NotaFiscal nf = builder.paraEmpresa("Caelum")
                .comCnpj("123.456.789/0001-10")
                .comItem(new ItemDaNota("item 1", 100.0))
                .comItem(new ItemDaNota("item 2", 200.0))
                .comItem(new ItemDaNota("item 3", 300.0))
                .comObservacoes("entregar notaFiscal pessoalmente")
                .naDataAtual()
                .build();

        System.out.println(nf);
    }

}
