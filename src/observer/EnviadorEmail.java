package observer;

public class EnviadorEmail implements AcaoAposGerarNota {

    @Override
    public void executa(NotaFiscal nf) {
        System.out.println("Enviando e-mail");
    }

}
