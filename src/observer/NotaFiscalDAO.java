package observer;

public class NotaFiscalDAO implements AcaoAposGerarNota {

    @Override
    public void executa(NotaFiscal nf) {
        System.out.println("Salvando NF no banco");
    }

}
