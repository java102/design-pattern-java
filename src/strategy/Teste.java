package strategy;

public class Teste {

    public static void main(String[] args) {
        Orcamento orcamento = new Orcamento(1500.0);
        Imposto icms = new ICMS();

        new CalculadorDeImposto().calcula(orcamento, icms);
    }

}
