package strategy;

public class CalculadorDeImposto {

    public void calcula(Orcamento orcamento, Imposto estrategiaDeImposto) {
        double resultado = estrategiaDeImposto.calcula(orcamento);
        System.out.println(resultado);
        // realizaria outras operações depois do cálculo, por exemplo, enviar e-mail de notificação.
    }

}
