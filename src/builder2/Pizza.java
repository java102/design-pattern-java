package builder2;

public class Pizza {

    private Integer tamanho;
    private Boolean queijo;
    private Boolean tomate;
    private Boolean bacon;

    private Pizza(Builder builder) {
        this.tamanho = builder.tamanho;
        this.queijo = builder.queijo;
        this.tomate = builder.tomate;
        this.bacon = builder.bacon;
    }

    public Integer getTamanho() {
        return tamanho;
    }

    public Boolean getQueijo() {
        return queijo;
    }

    public Boolean getTomate() {
        return tomate;
    }

    public Boolean getBacon() {
        return bacon;
    }

    @Override
    public String toString() {
        return "Pizza{" +
                "tamanho=" + tamanho +
                ", queijo=" + queijo +
                ", tomate=" + tomate +
                ", bacon=" + bacon +
                '}';
    }

    public static class Builder {

        private Integer tamanho;
        private Boolean queijo = false;
        private Boolean tomate = false;
        private Boolean bacon = false;

        public Builder queijo() {
            queijo = true;
            return this;
        }

        public Builder tamanho(Integer tamanho) {
            this.tamanho = tamanho;
            return this;
        }

        public Builder tomate() {
            tomate = true;
            return this;
        }

        public Builder bacon() {
            bacon = true;
            return this;
        }

        public Pizza build() {
            return new Pizza(this);
        }

    }

}
