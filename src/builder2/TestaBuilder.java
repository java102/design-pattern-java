package builder2;

public class TestaBuilder {

    public static void main(String[] args) {
        Pizza pizza = new Pizza.Builder().tamanho(10).queijo().bacon().build();
        System.out.println(pizza);
    }

}
