package memento;

import java.util.Calendar;

public class TestaMemento {

    public static void main(String[] args) {
        Historico historico = new Historico();

        Contrato contrato = new Contrato(Calendar.getInstance(), "Mauricio", TipoContrato.NOVO);
        historico.adiciona(contrato.salvaEstado());

        contrato.avanca();
        historico.adiciona(contrato.salvaEstado());

        contrato.avanca();
        historico.adiciona(contrato.salvaEstado());

        contrato.avanca();
        historico.adiciona(contrato.salvaEstado());

        //pegando o histórico
        contrato.restaura(historico.pega(1));

        // mostra o estado do contrato
        System.out.println(contrato);
    }

}
